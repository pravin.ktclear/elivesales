<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('customer_name')->nullable()->after('name');
            $table->string('customer_code')->nullable()->after('customer_name');
            $table->string('gst')->nullable()->after('company_name');
            $table->string('website')->nullable()->after('gst');
            $table->string('billing_address',250)->nullable()->after('phone_number');
            $table->string('shipping_address',250)->nullable()->after('billing_address');
            $table->integer('transport')->nullable()->after('shipping_address');
            $table->integer('source')->after('transport');
            $table->string('contact_person')->nullable()->after('source');
            $table->string('contact_person_mobile')->nullable()->after('contact_person');
            $table->string('contact_person_email')->nullable()->after('contact_person_mobile');
            $table->string('contact_person_mobile2')->nullable()->after('contact_person_email');
            $table->string('contact_person_email2')->nullable()->after('contact_person_mobile2');
            $table->string('remark')->nullable()->after('contact_person_email2');
            $table->integer('rating')->nullable()->after('remark');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            //
        });
    }
}
