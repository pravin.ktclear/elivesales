<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOpenStockWithOtherColumnToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('openingStock')->after('hsnId')->nullable();
            $table->integer('openingStockValue')->after('openingStock')->nullable();
            $table->integer('pcsBox')->after('openingStockValue')->nullable();
            $table->string('boxWeight')->after('pcsBox')->nullable();
            $table->string('boxDimensions')->after('boxWeight')->nullable();
            $table->string('cbm')->after('boxDimensions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
