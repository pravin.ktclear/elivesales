<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('QUERY_ID');
            $table->char('QTYPE', 4);
            $table->string('SENDERNAME');
            $table->string('SENDEREMAIL');
            $table->string('SUBJECT');
            $table->string('DATE_RE');
            $table->string('DATE_R');
            $table->string('DATE_TIME_RE');
            $table->string('GLUSR_USR_COMPANYNAME');
            $table->string('READ_STATUS');
            $table->string('SENDER_GLUSR_USR_ID');
            $table->string('MOB');
            $table->string('COUNTRY_FLAG');
            $table->string('QUERY_MODID');
            $table->string('LOG_TIME');
            $table->string('QUERY_MODREFID');
            $table->string('DIR_QUERY_MODREF_TYPE');
            $table->string('ORG_SENDER_GLUSR_ID');
            $table->string('ENQ_MESSAGE');
            $table->string('ENQ_ADDRESS');
            $table->string('ENQ_CALL_DURATION');
            $table->string('ENQ_RECEIVER_MOB');
            $table->string('ENQ_CITY');
            $table->string('ENQ_STATE');
            $table->string('PRODUCT_NAME');
            $table->string('COUNTRY_ISO');
            $table->string('EMAIL_ALT');
            $table->string('MOBILE_ALT');
            $table->string('PHONE');
            $table->string('PHONE_ALT');
            $table->string('IM_MEMBER_SINCE');
            $table->string('TOTAL_COUNT');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
