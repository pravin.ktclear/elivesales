<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInvoicenoInvoicedateToTablePurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases', function (Blueprint $table) {
            $table->date('invoice_date')->nullable()->after('reference_no');
            $table->string('invoice_no')->after('invoice_date');
            $table->double('igst_amt')->after('grand_total');
            $table->double('cgst_amt')->after('igst_amt');
            $table->double('sgst_amt')->after('cgst_amt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases', function (Blueprint $table) {
            //
        });
    }
}
