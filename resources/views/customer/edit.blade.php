@extends('layout.main') @section('content')
@if(session()->has('not_permitted'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div> 
@endif
<section class="forms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>{{trans('file.Update Customer')}}</h4>
                    </div>
                    <div class="card-body">
                        <p class="italic"><small>{{trans('file.The field labels marked with * are required input fields')}}.</small></p>
                        {!! Form::open(['route' => ['customer.update',$lims_customer_data->id], 'method' => 'put', 'files' => true]) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="customer_group" value="{{$lims_customer_data->customer_group_id}}">
                                    <label>{{trans('file.Customer Group')}} *</strong> </label>
                                    <select required class="form-control selectpicker" name="customer_group_id">
                                        @foreach($lims_customer_group_all as $customer_group)
                                            <option value="{{$customer_group->id}}">{{$customer_group->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.name')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('customer_code')}}</span> </label>
                                    <input type="text" name="customer_name" value="{{$lims_customer_data->name}}" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Customer Code')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('customer_code')}}</span></label>
                                    <input type="text" id="customer_code" name="customer_code" value="{{$lims_customer_data->customer_code}}" required class="form-control" onkeyup='saveValue(this);'>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Company Name')}} </label>
                                    <input type="text" name="company_name" value="{{$lims_customer_data->company_name}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.GST')}}</label> <span style="color:red;">{{$errors->first('gst')}}</span>
                                    <input type="text" name="gst" value="{{$lims_customer_data->gst}}" placeholder="GST NO" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.website')}}</label> 
                                    <input type="text" name="website" value="{{$lims_customer_data->website}}" placeholder="Paste your Website URL Here" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Email')}}</label>
                                    <input type="email" name="email" value="{{$lims_customer_data->email}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Phone Number')}} *</label>
                                    <input type="text" name="phone_number" required value="{{$lims_customer_data->phone_number}}" class="form-control">
                                    @if($errors->has('phone_number'))
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Billing Address')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('billing_address')}}</span></label>
                                    <input type="text" name="billing_address" value="{{$lims_customer_data->billing_address}}" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Shipping Address')}} <span style="color: red;">*</span><span style="color:red;">{{$errors->first('shipping_address')}}</span></label>
                                    <input type="text" name="shipping_address" value="{{$lims_customer_data->shipping_address}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Transport')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('transport')}}</span></label>
                                    <select required class="form-control selectpicker" id="customer-group-id" name="transport" onchange='saveValue(this);'>
                                        <option value="0">Select Transport</option>
                                        @foreach($transports as $transport)
                                            <!-- <option value="{{$transport->id}}">{{$transport->transport_name}}</option> -->
                                            <option {{ $lims_customer_data->transport == $transport->id ? "selected" : "" }} value="{{ $transport->id }}">{{$transport->transport_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Source')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('source')}}</span></label>
                                    <select required class="form-control selectpicker" id="customer-group-id" name="source" onchange='saveValue(this);'>
                                        <option value="0">Select Source</option>
                                        @foreach($sources as $source)
                                            <!-- <option value="{{$source->id}}">{{$source->source}}</option> -->
                                            <option {{ $lims_customer_data->source == $source->id ? "selected" : "" }} value="{{ $source->id }}">{{$source->source}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Contact Person')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('contact_person')}}</span></label>
                                    <input type="text" name="contact_person" value="{{$lims_customer_data->contact_person}}" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Contact Person Mobile')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('contact_person_mobile')}}</span></label>
                                    <input type="number" name="contact_person_mobile" value="{{$lims_customer_data->contact_person_mobile}}" required class="form-control" maxlength="10" pattern="\d{10}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Contact Person Email')}} <span style="color:red;">{{$errors->first('contact_person_email')}}</span></label>
                                    <input type="email" name="contact_person_email" value="{{$lims_customer_data->contact_person_email}}" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Contact Person Mobile 2')}} <span style="color:red;">{{$errors->first('contact_person_mobile2')}}</span></label>
                                    <input type="number" name="contact_person_mobile2" value="{{$lims_customer_data->contact_person_mobile2}}" required class="form-control" maxlength="10" pattern="\d{10}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Contact Person Email 2')}} <span style="color:red;">{{$errors->first('contact_person_email2')}}</span></label>
                                    <input type="email" name="contact_person_email2" value="{{$lims_customer_data->contact_person_email2}}" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Allot to User')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('user_id')}}</span></label>
                                    <div class="input-group">
                                          <select name="user_id" id="user_id" class="selectpicker form-control" data-live-search="true" data-live-search-style="begins" title="Select User to allocate this customer..">
                                            @foreach($users as $user)
                                                <!-- <option value="{{$user->id}}">{{$user->name}}</option> -->
                                                <option {{ $lims_customer_data->user_id == $user->id ? "selected" : "" }} value="{{ $user->id }}">{{$user->name}}</option>
                                            @endforeach
                                          </select>
                                      </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Postal Code')}} <span style="color: red;">*</span><span style="color:red;">{{$errors->first('postal_code')}}</span></label>
                                    <div class="input-group">
                                          <select name="postal_code" id="postal_code" class="selectpicker form-control" data-live-search="true" data-live-search-style="begins" title="Select Postal Code...">
                                            @foreach($pincodes as $pincode)
                                                <!-- <option value="{{$pincode->id}}">{{$pincode->pincode}} {{$pincode->Taluk}}</option> -->
                                                <option {{ $lims_customer_data->postal_code == $pincode->id ? "selected" : "" }} value="{{ $pincode->id }}">{{$pincode->pincode}} {{$pincode->Taluk}}</option>
                                            @endforeach
                                          </select>
                                      </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.City')}} <span style="color: red;">*</span></label>
                                    <input type="text" name="city" value="{{$lims_customer_data->city}}" id="city" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.State')}} <span style="color: red;">*</span></label>
                                    <input type="text" name="state" value="{{$lims_customer_data->state}}" id="state" class="form-control">
                                </div>
                            </div>
                            <!-- <div class="col-md-6 mt-3">
                                <div class="form-group">
                                    <label>{{trans('file.Add User')}}</label>&nbsp;
                                    <input type="checkbox" name="user" value="1" />
                                </div>
                            </div> -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Country')}}</label>
                                    <input type="text" name="country" value="{{$lims_customer_data->country}}" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Remark')}} </label>
                                    <input type="text" name="remark" value="{{$lims_customer_data->remark}}" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Rating')}} <span style="color: red;">*</span> <span style="color:red;">{{$errors->first('rating')}}</span></label>
                                    <select required class="form-control selectpicker" id="rating" name="rating" onchange='saveValue(this);'>
                                    @for ($i = 1; $i < 6; $i++)
                                       <option {{ $lims_customer_data->rating == $i ? "selected" : "" }} value="{{$i}}">{{$i}}</option>
                                     @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="is_active" value="0">
                                    <input type="checkbox" name="is_active" value="1" checked>
                                    <label class="mt-2"><strong>{{trans('file.Active')}}</strong></label>
                                </div>
                            </div>
                            <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Tax Number')}}</label>
                                    <input type="text" name="tax_no" class="form-control" value="{{$lims_customer_data->tax_no}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Address')}} *</label>
                                    <input type="text" name="address" required value="{{$lims_customer_data->address}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.City')}} *</label>
                                    <input type="text" name="city" required value="{{$lims_customer_data->city}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.State')}}</label>
                                    <input type="text" name="state" value="{{$lims_customer_data->state}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Postal Code')}}</label>
                                    <input type="text" name="postal_code" value="{{$lims_customer_data->postal_code}}" class="form-control">
                                </div>
                            </div>
                            @if(!$lims_customer_data->user_id)
                            <div class="col-md-6 mt-3">
                                <div class="form-group">
                                    <label>{{trans('file.Add User')}}</label>&nbsp;
                                    <input type="checkbox" name="user" value="1" />
                                </div>
                            </div>
                            @endif
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('file.Country')}}</label>
                                    <input type="text" name="country" value="{{$lims_customer_data->country}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6 user-input">
                                <div class="form-group">
                                    <label>{{trans('file.UserName')}} *</label>
                                    <input type="text" name="name" class="form-control">
                                    @if($errors->has('name'))
                                   <span>
                                       <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 user-input">
                                <div class="form-group">
                                    <label>{{trans('file.Password')}} *</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                            </div> -->
                            <div class="col-md-12">
                                <div class="form-group mt-3">
                                    <input type="submit" value="{{trans('file.submit')}}" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $("ul#people").siblings('a').attr('aria-expanded','true');
    $("ul#people").addClass("show");

    $(".user-input").hide();

    $('input[name="user"]').on('change', function() {
        if ($(this).is(':checked')) {
            $('.user-input').show(300);
            $('input[name="name"]').prop('required',true);
            $('input[name="password"]').prop('required',true);
        }
        else{
            $('.user-input').hide(300);
            $('input[name="name"]').prop('required',false);
            $('input[name="password"]').prop('required',false);
        }
    });

    $('#postal_code').on('change', function() {
        var id = $(this).val();
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "pincode.getData",
            type: "get",
            data: { 
                    id: id        
                  },
            cache: false,
            success: function(data){
              $('#city').val(data.Taluk);
              $('#state').val(data.statename);
              }
      });
    });
        
    var customer_group = $("input[name='customer_group']").val();
    $('select[name=customer_group_id]').val(customer_group);
</script>
@endsection