@extends('layout.main') @section('content')
@if(session()->has('message'))
  <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('message') }}</div> 
@endif
@if(session()->has('not_permitted'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div> 
@endif

@if (count($errors) > 0)
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#createModal').modal('show');
            $('#transport-modal').modal('show');
        });
    </script>
  @endif

<section>
    <div class="container-fluid">
        
        @if(in_array("transport-add", $all_permission))
            <button class="btn btn-info" data-toggle="modal" data-target="#transport-modal"><i class="dripicons-plus"></i> {{trans('file.Add Transport')}}</button>
        @endif
    </div>
    <div class="table-responsive">
        <table id="expense-table" class="table">
            <thead>
                <tr>
                    <th class="not-exported"></th>
                    <th>{{trans('file.Transport Name')}}</th>
                    <th>{{trans('file.Transport GST')}}</th>
                    <th>{{trans('file.Transport Person')}}</th>
                    <th>{{trans('file.Transport Mobile')}}</th>
                    <th>{{trans('file.Transport Address')}}</th>
                    <th>{{trans('file.status')}}</th>
                    <th class="not-exported">{{trans('file.action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($list as $key=>$transport)
                <tr data-id="{{$transport->id}}">
                    <td>{{$key}}</td>
                    <td>{{ $transport->transport_name }}</td>
                    <td>{{ $transport->transport_gst}}</td>
                    <td>{{ $transport->transport_person }}</td>
                    <td>{{ $transport->transport_mobile }}</td>
                    <td>{{ $transport->transport_address }}</td>
                    @if($transport->status == 'Active')
                    <td><div class="badge badge-success">Active</div></td>
                    @else
                    <td><div class="badge badge-danger">Inactive</div></td>
                    @endif
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('file.action')}}
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu edit-options dropdown-menu-right dropdown-default" user="menu">
                                @if(in_array("transport-edit", $all_permission))
                                <li><button type="button" data-id="{{$transport->id}}" class="open-EditTransport_categoryDialog btn btn-link" data-toggle="modal" data-target="#editModal"><i class="dripicons-document-edit"></i> {{trans('file.edit')}}</button></li>
                                @endif
                                @if(in_array("transport-delete", $all_permission))
                                <li class="divider"></li>
                                {{ Form::open(['route' => ['transport.destroy', $transport->id], 'method' => 'DELETE'] ) }}
                                <li>
                                    <button type="submit" class="btn btn-link" onclick="return confirmDelete()"><i class="dripicons-trash"></i> {{trans('file.delete')}}</button>
                                </li>
                                {{ Form::close() }}
                                @endif
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<div id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">{{trans('file.Update Transport')}}</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"><i class="dripicons-cross"></i></span></button>
            </div>
            <div class="modal-body">
              <p class="italic"><small>{{trans('file.The field labels marked with * are required input fields')}}.</small></p>
                {!! Form::open(['route' => ['transport.update', 1], 'method' => 'put']) !!}
                
                  <div class="form-group">
                      <input type="hidden" name="transport_id" id="transport_id">
                  </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>{{trans('file.Transport Name')}} <span style="color: red;">*</span></label>
                            <input type="text" name="transport_name" value="{{old('transport_name')}}" required class="form-control">
                            <span style="color:red;">{{$errors->first('transport_name')}}</span>
                        </div>
                        <div class="col-md-6 form-group">
                            <label>{{trans('file.Transport GST')}} <span style="color: red;">*</span></label>
                            <span style="color:red;">{{$errors->first('transport_gst')}}</span>
                            <input type="text" name="transport_gst" value="{{old('transport_gst')}}" class="form-control">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>{{trans('file.Transport Person')}} </label>
                            <span style="color:red;">{{$errors->first('transport_person')}}</span>
                            <input type="text" name="transport_person" value="{{old('transport_person')}}" class="form-control">
                        </div>
                        <div class="col-md-6 form-group">
                            <label> {{trans('file.Transport Mobile')}}<span style="color: red;">*</span></label>
                            <span style="color:red;">{{$errors->first('transport_mobile')}}</span>
                            <input type="text" name="transport_mobile" value="{{old('transport_mobile')}}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                      <label>{{trans('file.Transport Address')}}<span style="color: red;">*</span></label>
                      <textarea name="transport_address" rows="3" value="{{old('transport_address')}}" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                      <input type="hidden" name="status" value="0">
                      <input type="checkbox" name="status" value="1" checked>
                      <label class="mt-2"><strong>{{trans('file.Active')}}</strong></label>
                    </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-primary">{{trans('file.submit')}}</button>
                  </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $("ul#transport").siblings('a').attr('aria-expanded','true');
    $("ul#transport").addClass("show");
    $("ul#transport #trans-list-menu").addClass("active");

    var expense_id = [];
    var user_verified = <?php echo json_encode(env('USER_VERIFIED')) ?>;
    var all_permission = <?php echo json_encode($all_permission) ?>;
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".daterangepicker-field").daterangepicker({
      callback: function(startDate, endDate, period){
        var start_date = startDate.format('YYYY-MM-DD');
        var end_date = endDate.format('YYYY-MM-DD');
        var title = start_date + ' To ' + end_date;
        $(this).val(title);
        $('input[name="start_date"]').val(start_date);
        $('input[name="end_date"]').val(end_date);
      }
    });

    $(document).ready(function() {
        $(document).on('click', 'button.open-EditTransport_categoryDialog', function() {
            var url = "transport/";
            var id = $(this).data('id').toString();
            url = url.concat(id).concat("/edit");
            $.get(url, function(data) {
                $('#editModal #reference').text(data['reference_no']);
                $("#editModal input[name='transport_name']").val(data['transport_name']);
                $("#editModal input[name='transport_gst']").val(data['transport_gst']);
                $("#editModal input[name='transport_person']").val(data['transport_person']);
                $("#editModal input[name='transport_mobile']").val(data['transport_mobile']);
                $("#editModal input[name='transport_id']").val(data['id']);
                $("#editModal textarea[name='transport_address']").val(data['transport_address']);
                if(data['status'] == 'Active'){  
                    $("#editModal #status").prop('checked', true);
                }else{
                    $("#editModal #status").prop('checked', false);
                }
            });
        });
    });

function confirmDelete() {
    if (confirm("Are you sure want to delete?")) {
        return true;
    }
    return false;
}

    $('#expense-table').DataTable( {
        "order": [],
        'language': {
            'lengthMenu': '_MENU_ {{trans("file.records per page")}}',
             "info":      '<small>{{trans("file.Showing")}} _START_ - _END_ (_TOTAL_)</small>',
            "search":  '{{trans("file.Search")}}',
            'paginate': {
                    'previous': '<i class="dripicons-chevron-left"></i>',
                    'next': '<i class="dripicons-chevron-right"></i>'
            }
        },
        'columnDefs': [
            {
                "orderable": false,
                'targets': [0, 7]
            },
            {
                'render': function(data, type, row, meta){
                    if(type === 'display'){
                        data = '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
                    }

                   return data;
                },
                'checkboxes': {
                   'selectRow': true,
                   'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                },
                'targets': [0]
            }
        ],
        'select': { style: 'multi',  selector: 'td:first-child'},
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: '<"row"lfB>rtip',
        buttons: [
            {
                extend: 'pdf',
                text: '<i title="export to pdf" class="fa fa-file-pdf-o"></i>',
                exportOptions: {
                    columns: ':visible:Not(.not-exported)',
                    rows: ':visible'
                },
                action: function(e, dt, button, config) {
                    datatable_sum(dt, true);
                    $.fn.dataTable.ext.buttons.pdfHtml5.action.call(this, e, dt, button, config);
                    datatable_sum(dt, false);
                },
                footer:true
            },
            {
                extend: 'csv',
                text: '<i title="export to csv" class="fa fa-file-text-o"></i>',
                exportOptions: {
                    columns: ':visible:Not(.not-exported)',
                    rows: ':visible'
                },
                action: function(e, dt, button, config) {
                    datatable_sum(dt, true);
                    $.fn.dataTable.ext.buttons.csvHtml5.action.call(this, e, dt, button, config);
                    datatable_sum(dt, false);
                },
                footer:true
            },
            {
                extend: 'print',
                text: '<i title="print" class="fa fa-print"></i>',
                exportOptions: {
                    columns: ':visible:Not(.not-exported)',
                    rows: ':visible'
                },
                action: function(e, dt, button, config) {
                    datatable_sum(dt, true);
                    $.fn.dataTable.ext.buttons.print.action.call(this, e, dt, button, config);
                    datatable_sum(dt, false);
                },
                footer:true
            },
            {
                text: '<i title="delete" class="dripicons-cross"></i>',
                className: 'buttons-delete',
                action: function ( e, dt, node, config ) {
                    if(user_verified == '1') {
                        expense_id.length = 0;
                        $(':checkbox:checked').each(function(i){
                            if(i){
                                expense_id[i-1] = $(this).closest('tr').data('id');
                            }
                        });
                        if(expense_id.length && confirm("Are you sure want to delete?")) {
                            $.ajax({
                                type:'POST',
                                url:'transports/deletebyselection',
                                data:{
                                    expenseIdArray: expense_id
                                },
                                success:function(data){
                                   window.location.href = "/transport";
                                }
                            });
                            dt.rows({ page: 'current', selected: true }).remove().draw(false);
                        }
                        else if(!expense_id.length)
                            alert('No Transport is selected!');
                    }
                    else
                        alert('This feature is disable for demo!');
                }
            },
            {
                extend: 'colvis',
                text: '<i title="column visibility" class="fa fa-eye"></i>',
                columns: ':gt(0)'
            },
        ],
        drawCallback: function () {
            var api = this.api();
            datatable_sum(api, false);
        }
    } );

    function datatable_sum(dt_selector, is_calling_first) {
        if (dt_selector.rows( '.selected' ).any() && is_calling_first) {
            var rows = dt_selector.rows( '.selected' ).indexes();
            $( dt_selector.column( 5 ).footer() ).html(dt_selector.cells( rows, 5, { page: 'current' } ).data().sum().toFixed(2));
        }
        else {
            $( dt_selector.column( 5 ).footer() ).html(dt_selector.cells( rows, 5, { page: 'current' } ).data().sum().toFixed(2));
        }
    }

    if(all_permission.indexOf("expenses-delete") == -1)
        $('.buttons-delete').addClass('d-none');

</script>
@endsection