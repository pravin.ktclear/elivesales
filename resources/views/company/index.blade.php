@extends('layout.main') @section('content')
@if(session()->has('message'))
  <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('message') }}</div> 
@endif
@if(session()->has('not_permitted'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div> 
@endif

@if (count($errors) > 0)
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#transport-modal').modal('hide');
            $('#editModal').modal('show');
          
        });
    </script>
  @endif

<section>
    <div class="container-fluid">
        <div class="card">
            <div class="card-header text-center">
                <div class="row">
                    <div class="col-md-9">
                        <h3>Company Profile</h3>
                    </div>
                    <div class="col-md-3">
                        @if(in_array("source-add", $all_permission))
                            <button class="btn btn-info" data-toggle="modal" data-target="#editModal"><i class="dripicons-document-edit"></i> {{trans('file.Edit Company')}}</button>
                        @endif
                    </div>
                </div>
            
          </div>
          <div class="card-body">
            <h5 class="card-title">Company Information</h5>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <label><b>{{trans('Company Name')}} : </b></label> <span>{{$company->company_name}}</span>
                </div>
                <div class="col-md-4">
                    <label><b>{{trans('Proprietary Name')}} :</b></label> <span>{{$company->proprietary_name}}</span>
                </div>
                <div class="col-md-4">
                    <label><b>{{trans('Office Address')}} :</b></label> <span>{{$company->office_address}}</span>
                </div>
            </div>
            <hr>
            <div class="row">    
                <div class="col-md-4">
                    <label><b>{{trans('Reg. Address')}} :</b></label> <span>{{$company->registered_address}}</span>
                </div>
                <div class="col-md-4">
                    <label><b>{{trans('Company Email')}} :</b></label> <span>{{$company->email}}</span>
                </div>
                <div class="col-md-4">
                    <label><b>{{trans('Company GST No')}} :</b></label> <span>{{$company->gst_no}}</span>
                </div>
            </div>
            <hr>
            <div class="row">    
                <div class="col-md-4">
                    <label><b>{{trans('Company PAN No')}} :</b></label> <span>{{$company->pan_no}}</span>
                </div>
                <div class="col-md-4">
                    <label><b>{{trans('Contact No ')}}:</b></label> <span>{{$company->contact_no_1}}</span>
                </div>
                <div class="col-md-4">
                    <label><b>{{trans('Contact No 2')}} :</b></label> <span>{{$company->contact_no_2}}</span>
                </div>
            </div>
            <hr>
            <div class="row">    
                <div class="col-md-4">
                    <label><b>{{trans('Company Bank Details ')}}:</b></label> <span>{{$company->bank_details}}</span>
                </div>
                <div class="col-md-4">
                    <label><b>{{trans('State')}} :</b></label> <span>{{$company->state}}</span>
                </div>
                <div class="col-md-4">
                    <label><b>{{trans('Status')}} :</b></label> <span>{{ $company->is_active == 1 ? 'Active' : 'In Active' }}</span>
                </div>
            </div>
            
          </div>
          <div class="card-footer text-muted">
            
          </div>
        </div>
    </div>   
</section>

<div id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['route' => 'company.update', 'method' => 'post']) !!}

            <input type="hidden" value="{{ Auth::user()->id }}" name="created_by">
            <input type="hidden" value="{{ $company->id }}" name="id">
            <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">{{trans('Edit Company')}}</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true"><i class="dripicons-cross"></i></span></button>
            </div>
            <div class="modal-body">
                <p class="italic"><small>{{trans('file.The field labels marked with * are required input fields')}}.</small></p>
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Company Name')}} *</label><span style="color:red;">{{$errors->first('company_name')}}</span>
                                {{Form::text('company_name',$company->company_name,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Proprietary Name')}} *</label><span style="color:red;">{{$errors->first('proprietary_name')}}</span>
                                {{Form::text('proprietary_name',$company->proprietary_name,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Office Address')}} *</label><span style="color:red;">{{$errors->first('office_address')}}</span>
                                {{Form::text('office_address',$company->office_address,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Reg. Address')}} *</label><span style="color:red;">{{$errors->first('registered_address')}}</span>
                                {{Form::text('registered_address',$company->registered_address,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Company Email')}} *</label><span style="color:red;">{{$errors->first('email')}}</span>
                                {{Form::text('email',$company->email,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Company GST No')}} *</label><span style="color:red;">{{$errors->first('gst_no')}}</span>
                                {{Form::text('gst_no',$company->gst_no,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Company PAN No')}} *</label><span style="color:red;">{{$errors->first('pan_no')}}</span>
                                {{Form::text('pan_no',$company->pan_no,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Contact No')}} *</label><span style="color:red;">{{$errors->first('contact_no_1')}}</span>
                                {{Form::text('contact_no_1',$company->contact_no_1,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Contact No 2')}} *</label><span style="color:red;">{{$errors->first('contact_no_2')}}</span>
                                {{Form::text('contact_no_2',$company->contact_no_2,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('Company Bank Details')}} *</label><span style="color:red;">{{$errors->first('bank_details')}}</span>
                                {{Form::text('bank_details',$company->bank_details,array('class' => 'form-control'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{trans('State')}} *</label><span style="color:red;">{{$errors->first('state')}}</span>
                                {{Form::text('state',$company->state,array('required' => 'required', 'class' => 'form-control'))}}
                            </div>
                        </div>
                    </div>   
                    <input type="submit" value="{{trans('file.submit')}}" class="btn btn-primary">
                </form>
            </div>
        {{ Form::close() }}
        </div>
    </div>
</div>

<script type="text/javascript">

</script>
@endsection