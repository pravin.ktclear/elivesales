@extends('layout.main') @section('content')

@if(session()->has('not_permitted'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div> 
@endif
<section class="forms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>{{trans('file.AddYear')}}</h4>
                    </div>
                    <div class="card-body">
                        <p class="italic"><small>{{trans('file.The field labels marked with * are required input fields')}}.</small></p>
                        {!! Form::open(['route' => 'year.store', 'method' => 'post', 'files' => true]) !!}
                            <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><strong>{{trans('file.AddYear')}}</strong> <span style="color: red;">*</span><span style="color:red;">{{$errors->first('customer_name')}}</span></label>
                                        <input type="text" name="year_name" value="{{ old('year_name') }}" required class="form-control">    
                                    </div>
                                </div>                             
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><strong>{{trans('file.Start Date')}}</strong> <span style="color: red;">*</span><span style="color:red;">{{$errors->first('start_date')}}</span></label>
                                        <input type="date" name="start_date" value="{{ old('start_date') }}" required class="form-control">    
                                    </div>
                                </div>                             
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><strong>{{trans('file.End Date')}}</strong> <span style="color: red;">*</span><span style="color:red;">{{$errors->first('end_date')}}</span></label>
                                        <input type="date" name="end_date" value="{{ old('end_date') }}" required class="form-control">    
                                    </div>
                                </div>                             
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="hidden" name="status" value="0">
                                        <input type="checkbox" name="status" value="1" checked>
                                        <label class="mt-2"><strong>{{trans('file.Active')}}</strong></label>
                                    </div>
                                </div>
                            </div>    
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-tc mt-2"><strong>{{trans('file.Choose Your Date')}}</strong> <span style="color: red;">*</span><span style="color:red;">{{$errors->first('daterangepicker')}}</span></label>
                                        <input type="text" class="daterangepicker-field form-control" value="{{ \Carbon\Carbon::now()->startOfMonth() }} - {{ \Carbon\Carbon::now()->endOfMonth() }}" required />
                                        <input type="hidden" name="start_date" value="{{ \Carbon\Carbon::now()->startOfMonth() }}" />
                                        <input type="hidden" name="end_date" value="{{ \Carbon\Carbon::now()->endOfMonth() }}" />
                                    </div>
                                </div>
                            </div> -->
                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="submit" value="{{trans('file.submit')}}" class="btn btn-primary">
                                    </div> 
                                </div>
                            </div>        
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $("ul#people").siblings('a').attr('aria-expanded','true');
    $("ul#people").addClass("show");
    $("ul#people #user-create-menu").addClass("active");

    $('#warehouseId').hide();
    $('#biller-id').hide();
    $('.customer-section').hide();

    $('.selectpicker').selectpicker({
      style: 'btn-link',
    });
    
    $('#genbutton').on("click", function(){
      $.get('genpass', function(data){
        $("input[name='password']").val(data);
      });
    });

    $('select[name="role_id"]').on('change', function() {
        if($(this).val() == 5) {
            $('#biller-id').hide(300);
            $('#warehouseId').hide(300);
            $('.customer-section').show(300);
            $('.customer-input').prop('required',true);
            $('select[name="warehouse_id"]').prop('required',false);
            $('select[name="biller_id"]').prop('required',false);
        }
        else if($(this).val() > 2 && $(this).val() != 5) {
            $('select[name="warehouse_id"]').prop('required',true);
            $('select[name="biller_id"]').prop('required',true);
            $('#biller-id').show(300);
            $('#warehouseId').show(300);
            $('.customer-section').hide(300);
            $('.customer-input').prop('required',false);
        }
        else {
            $('select[name="warehouse_id"]').prop('required',false);
            $('select[name="biller_id"]').prop('required',false);
            $('#biller-id').hide(300);
            $('#warehouseId').hide(300);
            $('.customer-section').hide(300);
            $('.customer-input').prop('required',false);
        }
    });
</script>
@endsection