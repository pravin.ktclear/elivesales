<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;
    protected $fillable =[
        'product_id','qty','rate','warehouse_id'
    ];

    public function products()
    {
    	return $this->hasMany('App\Product');
    }
}
