<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LaborTransfer extends Model
{
    protected $fillable =[

        "inward_outward_id", "product_id",  "qty", "purchase_unit_id", "net_unit_cost", "tax_rate", "tax", "total",'inventory_id'
    ];

}
