<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Labor extends Model
{
    use HasFactory;
    protected $fillable =[

        "name", "phone", "email", "address", "is_active"
    ];

    public function inventory(){
        return $this->hasMany('App\Inventory');
    }


}
