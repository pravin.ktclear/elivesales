<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable =[

        "name", "image", "company_name", "vat_number",
        "email", "phone_number", "address", "city",
        "state", "postal_code", "country", "is_active",
        "gstno","contact_person_name","contact_person_email","contact_person_phone_number"

    ];

    public function product()
    {
    	return $this->hasMany('App/Product');

    }
}
