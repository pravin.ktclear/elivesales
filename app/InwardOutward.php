<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InwardOutward extends Model
{
    protected $fillable =[

        "reference_no", "user_id", "status", "labor_id",  "item", "total_qty", "total_tax", "total_cost", "shipping_cost", "grand_total", "document", "note",'warehouse_id'
    ];

    public function labor(){
        return $this->belongsTo('App\Labor');
    }
    public function fromWarehouse()
    {
    	return $this->belongsTo('App\Warehouse', 'warehouse_id');
    }
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
