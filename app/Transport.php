<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Tax;

class Transport extends Model
{
    use HasFactory;
    protected $table = 'transport';
    protected $guarded=['submit'];

    public function scopeActive($query){
		return $query->where('status','=',1)->orderBy('created_at', 'desc')->get();
	}

	public function getStatusAttribute($attribute){
		return [
		0 => 'Inactive',
		1=>'Active'
		][$attribute];
	}
}
