<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPriceHistory extends Model
{
    use HasFactory;
    protected $table = 'productPriceHistory';

    protected $fillable = [
            'productId','wholesalePrice','retailPrice','effectFrom'
    ];

    public function product()
    {
    	return $this->hasMany('App\Product');
    }
}
