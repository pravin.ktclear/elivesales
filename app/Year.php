<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    use HasFactory;
    protected $table = 'years';
    protected $primaryKey = 'id';

    protected $guarded = [];
    
    public function getStatusAttribute($attribute){
		return [
		0 => 'Inactive',
		1=>'Active'
		][$attribute];
	}

}
