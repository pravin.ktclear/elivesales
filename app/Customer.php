<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable =[
        "customer_group_id", "user_id", "customer_name", "customer_code","company_name","gst" ,"website" ,
        "email", "phone_number","billing_address" ,"shipping_address" , "transport", "source", "contact_person",
        "contact_person_mobile", "contact_person_email", "contact_person_mobile2", "contact_person_email2", "user_id", "postal_code", "city", "state", "country",
        "remark", "rating", "is_active"
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
