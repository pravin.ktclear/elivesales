<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hsn extends Model
{
    protected $fillable =[

        "hsnId", 'hsnCode', "tax_id", "is_active"
    ];
    protected $table = 'hsn';
    protected $primaryKey = 'hsnId';

    public function product()
    {
    	return $this->hasMany('App\Product');
    }
    public function tax()
    {
        return $this->belongsTo('App\Tax');
    }
}
