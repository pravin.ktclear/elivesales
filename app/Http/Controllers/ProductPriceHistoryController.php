<?php

namespace App\Http\Controllers;

use App\Http\Requests\productPriceHistoryRequest;
use App\Product;
use App\ProductPriceHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Auth;
use COM;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class ProductPriceHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(productPriceHistoryRequest $request)
    {


        $input = $request->all();
        $priceData = ProductPriceHistory::where('productId', $request->id)->orderBy('id', 'DESC')->take(1);
        $updateProductPrice = Product::findOrFail($request->id);
        $updateProductPrice->update([
            'cost'=> $input['wholesalePrice'],
            'price'=> $input['retailPrice']
        ]);

        $input['productId'] = $request->id;
        $input['effectFrom'] = Carbon::now();
        $input['created_by'] = Auth::user()->id;

        // get Existing Record //
        $priceData->update([
            'effectTo' => $input['effectFrom'],
            'created_by' => $input['created_by']
        ]);
        ProductPriceHistory::create($input);

        return redirect('products')->with('message', 'Price inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductPriceHistory  $productPriceHistory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductPriceHistory $productPriceHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductPriceHistory  $productPriceHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductPriceHistory $productPriceHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductPriceHistory  $productPriceHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductPriceHistory $productPriceHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductPriceHistory  $productPriceHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductPriceHistory $productPriceHistory)
    {
        //
    }
}
