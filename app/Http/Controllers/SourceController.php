<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Source;
use Spatie\Permission\Models\Role;
use App\Http\Requests\sourceRequest;
use Spatie\Permission\Models\Permission;
use Auth;


class SourceController extends Controller
{
    
    public function index()
    {
        $list = Source::all();
        $role = Role::find(Auth::user()->role_id);
        if($role->hasPermissionTo('expenses-index')){
            $permissions = Role::findByName($role->name)->permissions;
            foreach ($permissions as $permission)
                $all_permission[] = $permission->name;
            if(empty($all_permission))
                $all_permission[] = 'dummy text';
        return view('source.index',compact('list','all_permission'));
        }
    }

    public function create()
    {
        
    }

    public function store(sourceRequest $request)
    {
        Source::create($request->All());
        return redirect('source')->with('message', 'Source Created Successfully');
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $role = Role::firstOrCreate(['id' => Auth::user()->role_id]);
        if ($role->hasPermissionTo('source-edit')) {
            $source = Source::find($id);
            return $source;
        }
        else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $source = Source::find($data['source_id']);
        $source->update($data);
        return redirect('source')->with('message', 'Source updated successfully');
    }
    
    public function destroy($id)
    {
        $record = Source::FindOrFail($id);
        $record->delete();
        return redirect('source')->with('not_permitted', 'Source deleted successfully');
    }

    public function deleteBySelection(Request $request)
    {
        $records = $request['expenseIdArray'];
        array_shift($records);
        foreach ($records as $id) {
            $lims_transport_data = Source::find($id);
            $lims_transport_data->delete();
        }
        return 'Sources deleted successfully!';
    }
}
