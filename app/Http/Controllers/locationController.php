<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hsn;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;
use App\City;

class locationController extends Controller
{
    
    public function index()
    {

    }

    public function create()
    {

    }


    public function store(Request $request)
    {
        
    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        
    }

    public function show(Request $request)
    {
       $city = City::findOrFail($request->id);
       return $city;
    }

}
