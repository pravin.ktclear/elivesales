<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warehouse;
use App\Product;
use App\Product_Warehouse;
use App\Tax;
use App\Unit;
use App\Transfer;
use App\ProductTransfer;
use App\ProductVariant;
use App\ProductBatch;
use Auth;
use App\Inventory;
use App\InwardOutward;
use App\Labor;
use App\LaborTransfer;
use DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;

class InwardOutwardController extends Controller
{

    public function index()
    {
        $role = Role::find(Auth::user()->role_id);
        if ($role->hasPermissionTo('transfers-index')) {
            $permissions = Role::findByName($role->name)->permissions;
            foreach ($permissions as $permission)
                $all_permission[] = $permission->name;
            if (empty($all_permission))
                $all_permission[] = 'dummy text';

            if (Auth::user()->role_id > 2 && config('staff_access') == 'own')
                $lims_transfer_all = InwardOutward::with('fromWarehouse','labor','user')->orderBy('id', 'desc')->where('user_id', Auth::id())->get();
            else
                $lims_transfer_all = InwardOutward::with('fromWarehouse','labor', 'user')->orderBy('id', 'desc')->get();
            //dd($lims_transfer_all);
            return view('inwardoutward.index', compact('lims_transfer_all', 'all_permission'));
        } else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function create()
    {
        $role = Role::find(Auth::user()->role_id);
        if ($role->hasPermissionTo('transfers-add')) {
            $lims_warehouse_list = Warehouse::where('is_active', true)->get();
            $lims_labor_list = Labor::where('is_active', true)->get();
            return view('inwardoutward.create', compact('lims_warehouse_list','lims_labor_list'));
        } else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }
    public function getProduct($id)
    {
        $lims_product_warehouse_data = Inventory::join('products', 'inventories.product_id', '=', 'products.id')
            ->where([
                ['products.is_active', true],
                ['inventories.warehouse_id', $id],
                ['inventories.qty', '>', 0]
            ])
            ->select('products.*', 'inventories.id as invId', 'inventories.qty as iQty', 'inventories.rate')
            ->get();



        config()->set('database.connections.mysql.strict', false);
        \DB::reconnect(); //important as the existing connection if any would be in strict mode

        $product_code = [];
        $product_name = [];
        $product_qty = [];
        $product_price = [];
        $product_data = [];
        $product_type = [];
        $product_id = [];
        $qty_list = [];
        $product_list = [];
        $inventory_id = [];
        //product without variant
        foreach ($lims_product_warehouse_data as $product_warehouse) {
            $product_qty[] = $product_warehouse->iQty;
            $product_price[] = $product_warehouse->rate;
            $product_code[] =  $product_warehouse->code;
            $product_name[] = htmlspecialchars($product_warehouse->name);
            $product_type[] = $product_warehouse->type;
            $product_id[] = $product_warehouse->id;
            $product_list[] = $product_warehouse->product_list;
            $qty_list[] = $product_warehouse->qty_list;
            $inventory_id[] = $product_warehouse->invId;
        }

        $product_data = [$product_code, $product_name, $product_qty, $product_type, $product_id, $product_list, $qty_list, $product_price, $inventory_id];
        return $product_data;
    }

    public function limsProductSearch(Request $request)
    {
        $product_code = explode("(", $request['data']);
        $product_code[0] = rtrim($product_code[0], " ");
        $product_variant_id = null;
        $lims_product_data = Product::where([
            ['code', $product_code[0]],
            ['is_active', true]
        ])->first();
        if (!$lims_product_data) {
            $lims_product_data = Product::join('inventories', 'inventories.product_id', '=', 'products.id')
                ->select('products.*', 'inventories.qty as iQty', 'inventories.rate as rate', 'inventories.id as invId')
                ->where([
                    ['inventories.id', $product_code],
                    ['products.is_active', true]
                ])->first();
        }
        $product['name'] = $lims_product_data->name;
        $product['code'] = $lims_product_data->code;


        $product['product_id'] = $lims_product_data->id;
        $product['qty'] = $lims_product_data->iQty;
        $product['inventory_id'] = $lims_product_data->invId;
        $product['cc_attached'] = $lims_product_data->cc_attached;
        $product['no_ofboxes'] = $lims_product_data->no_ofboxes;
        $product['godown_delvery'] = $lims_product_data->godown_delvery;
        return $product;
    }

    public function store(Request $request)
    {
        $data = $request->except('document');

        // return dd($data);
        $data['user_id'] = Auth::id();
        $data['reference_no'] = 'tr-' . date("Ymd") . '-' . date("his");
        $data['status'] =1;
        $data['total_tax']=0;
        $data['total_cost']=0;
        $data['grand_total']=0;


        $lims_transfer_data = InwardOutward::create($data);
        $inventory_id = $data['inventory_id'];
        $product_id = $data['product_id'];
        $product_code = $data['product_code'];
        $qty = $data['qty'];
        $product_transfer = [];



        foreach ($product_id as $i => $id) {
            $lims_product_data = Product::find($id);
            $lim_invemtory_data = Inventory::find($inventory_id[$i]);


            //get product data
            $quantity = 0;

            $product_transfer['inward_outward_id'] = $lims_transfer_data->id;
            $product_transfer['product_id'] = $id;
            $product_transfer['qty'] = $qty[$i];
            $product_transfer['purchase_unit_id'] = 0;
            $product_transfer['net_unit_cost'] = $lim_invemtory_data->rate;
            $product_transfer['tax_rate'] = 0;
            $product_transfer['tax'] = 0;
            $product_transfer['total'] = $lim_invemtory_data->rate * $qty[$i];
            $product_transfer['inventory_id'] = $inventory_id[$i];
            $lims_product_data->qty = $lims_product_data->qty - $qty[$i];
            $lims_product_data->save();
            $lim_invemtory_data->qty = $lim_invemtory_data->qty - $qty[$i];
            $lim_invemtory_data->save();
            LaborTransfer::create($product_transfer);

            // $lims_inventory = Inventory::firstOrNew( ['product_id'=>$id,'rate'=>$lim_invemtory_data->rate,'warehouse_id'=>$data['to_warehouse_id']]);
            // $lims_inventory->qty = ($lims_inventory->qty + $qty[$i]);
            // $lims_inventory->save();
        }

        return redirect('inward')->with('message', 'Product Outward created successfully');
    }

    public function productTransferData($id)
    {
        $lims_product_transfer_data = LaborTransfer::where('inward_outward_id', $id)->get();
        foreach ($lims_product_transfer_data as $key => $product_transfer_data) {
            $product = Product::find($product_transfer_data->product_id);
            $product_transfer[0][$key] = $product->name . ' [' . $product->code . ']';
            $product_transfer[1][$key] = $product_transfer_data->qty;

        }
        return $product_transfer;
    }

    public function edit($id)
    {
        $role = Role::find(Auth::user()->role_id);
        if ($role->hasPermissionTo('transfers-edit')) {
            $lims_warehouse_list = Warehouse::where('is_active', true)->get();
            $lims_transfer_data = Transfer::find($id);
            $lims_product_transfer_data = ProductTransfer::where('transfer_id', $id)->get();
            return view('inwardoutward.edit', compact('lims_warehouse_list', 'lims_transfer_data', 'lims_product_transfer_data'));
        } else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function updateStatus(Request $request){
        $id = $request->inward_outward_id;
        $lims_transfer_data = InwardOutward::find($id);
        $lims_product_transfer_data = LaborTransfer::where('inward_outward_id', $id)->get();
        $lims_transfer_data->status = 2;
        foreach ($lims_product_transfer_data as $product_transfer_data) {
            $lims_reProduct_inventory = Inventory::find($product_transfer_data->inventory_id);
            $lims_reProduct_inventory->qty = ($lims_reProduct_inventory->qty + $product_transfer_data->qty);
            $lims_reProduct_inventory->save();
            //$product_transfer_data->delete();
        }
        $lims_transfer_data->save();
        return redirect('inward')->with('not_permitted', 'Data Inward successfully');
    }

    // public function deleteBySelection(Request $request)
    // {
    //     $transfer_id = $request['transferIdArray'];
    //     foreach ($transfer_id as $id) {
    //         $lims_transfer_data = Transfer::find($id);
    //         $lims_product_transfer_data = ProductTransfer::where('transfer_id', $id)->get();
    //         foreach ($lims_product_transfer_data as $product_transfer_data) {
    //             $lims_transfer_unit_data = Unit::find($product_transfer_data->purchase_unit_id);
    //             if ($lims_transfer_unit_data->operator == '*') {
    //                 $quantity = $product_transfer_data->qty * $lims_transfer_unit_data->operation_value;
    //             } else {
    //                 $quantity = $product_transfer_data / $lims_transfer_unit_data->operation_value;
    //             }

    //             if ($lims_transfer_data->status == 1) {
    //                 //add quantity for from warehouse
    //                 if ($product_transfer_data->variant_id)
    //                     $lims_product_warehouse_data = Product_Warehouse::FindProductWithVariant($product_transfer_data->product_id, $product_transfer_data->variant_id, $lims_transfer_data->from_warehouse_id)->first();
    //                 else
    //                     $lims_product_warehouse_data = Product_Warehouse::FindProductWithoutVariant($product_transfer_data->product_id, $lims_transfer_data->from_warehouse_id)->first();
    //                 $lims_product_warehouse_data->qty += $quantity;
    //                 $lims_product_warehouse_data->save();
    //                 //deduct quantity for to warehouse
    //                 if ($product_transfer_data->variant_id)
    //                     $lims_product_warehouse_data = Product_Warehouse::FindProductWithVariant($product_transfer_data->product_id, $product_transfer_data->variant_id, $lims_transfer_data->to_warehouse_id)->first();
    //                 else
    //                     $lims_product_warehouse_data = Product_Warehouse::FindProductWithoutVariant($product_transfer_data->product_id, $lims_transfer_data->to_warehouse_id)->first();

    //                 $lims_product_warehouse_data->qty -= $quantity;
    //                 $lims_product_warehouse_data->save();
    //             } elseif ($lims_transfer_data->status == 3) {
    //                 //add quantity for from warehouse
    //                 if ($product_transfer_data->variant_id)
    //                     $lims_product_warehouse_data = Product_Warehouse::FindProductWithVariant($product_transfer_data->product_id, $product_transfer_data->variant_id, $lims_transfer_data->from_warehouse_id)->first();
    //                 else
    //                     $lims_product_warehouse_data = Product_Warehouse::FindProductWithoutVariant($product_transfer_data->product_id, $lims_transfer_data->from_warehouse_id)->first();

    //                 $lims_product_warehouse_data->qty += $quantity;
    //                 $lims_product_warehouse_data->save();
    //             }
    //             $product_transfer_data->delete();
    //         }
    //         $lims_transfer_data->delete();
    //     }
    //     return 'Transfer deleted successfully!';
    // }

    public function destroy($id)
    {
        $lims_transfer_data = InwardOutward::find($id);
        $lims_product_transfer_data = LaborTransfer::where('inward_outward_id', $id)->get();

        foreach ($lims_product_transfer_data as $product_transfer_data) {
            $lims_reProduct_inventory = Inventory::find($product_transfer_data->inventory_id);
            $lims_reProduct_inventory->qty = ($lims_reProduct_inventory->qty + $product_transfer_data->qty);
            $lims_reProduct_inventory->save();
            $product_transfer_data->delete();
        }
        $lims_transfer_data->delete();
        return redirect('inward')->with('not_permitted', 'Outward deleted successfully');
    }
}
