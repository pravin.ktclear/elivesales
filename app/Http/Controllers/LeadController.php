<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$response = Http::get('https://mapi.indiamart.com/wservce/enquiry/listing/GLUSR_MOBILE/9820186687/GLUSR_MOBILE_KEY/MTYzMTkwNjMyNS42NTI5IzEzNDUzODUz/Start_Time/30-JAN-2022/End_Time/31-JAN-2022/');

        $response = Http::get('https://mapi.indiamart.com/wservce/enquiry/listing/GLUSR_MOBILE/9920378990/GLUSR_MOBILE_KEY/MTY0NDY0MjkxMC43ODUyIzc3MDA2Njgz/Start_Time/30-JAN-2022/End_Time/31-JAN-2022/');
   
        $jsonData = $response->json();
         
        dd($jsonData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        //
    }
}
