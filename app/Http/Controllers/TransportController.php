<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\transportRequest;
use App\Transport;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;

class TransportController extends Controller
{
    public function index()
    {
        $list = Transport::all();
        $role = Role::find(Auth::user()->role_id);
        if($role->hasPermissionTo('expenses-index')){
            $permissions = Role::findByName($role->name)->permissions;
            foreach ($permissions as $permission)
                $all_permission[] = $permission->name;
            if(empty($all_permission))
                $all_permission[] = 'dummy text';
        return view('transport.index',compact('list','all_permission'));
    	}
	}

    public function create()
    {
    }

    public function store(transportRequest $request)
    {
       	Transport::create($request->All());
        return redirect('transport');
    }
    
    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $role = Role::firstOrCreate(['id' => Auth::user()->role_id]);
        if ($role->hasPermissionTo('transport-edit')) {
            $lims_transport_data = Transport::find($id);
            return $lims_transport_data;
        }
        else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $lims_transport_data = Transport::find($data['transport_id']);
        $lims_transport_data->update($data);
        return redirect('transport')->with('message', 'Transport updated successfully');
    }

    public function destroy($id)
    {
       $record = Transport::FindOrFail($id);
       $record->delete();
       return redirect('transport')->with('not_permitted', 'Transport deleted successfully');
    }

    public function deleteBySelection(Request $request)
    {
        $records = $request['expenseIdArray'];
        array_shift($records);
        foreach ($records as $id) {
            $lims_transport_data = Transport::find($id);
            $lims_transport_data->delete();
        }
        return 'Transports deleted successfully!';
    }
}
