<?php

namespace App\Http\Controllers;
use Auth;
use App\Year;
use Illuminate\Http\Request;
use App\Http\Requests\yearRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class YearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Year::all();
        $role = Role::find(Auth::user()->role_id);
        if($role->hasPermissionTo('year-index')){
            $permissions = Role::findByName($role->name)->permissions;
            foreach ($permissions as $permission)
                $all_permission[] = $permission->name;
            if(empty($all_permission))
                $all_permission[] = 'dummy text';
            return view('year.index',compact('list','all_permission'));
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('year.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(yearRequest $request)
    {
        if($request->status == 1){
            Year::where('status', 1)->update(['status'=>0]);
        }
        $result = Year::create($request->all());
        if($result)
            return redirect('year')->with('create_message', 'Financial Year Created Successfully');
        else
            return redirect('year')->with('create_message', 'Something went Wrong !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Year  $year
     * @return \Illuminate\Http\Response
     */
    public function show(Year $year)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Year  $year
     * @return \Illuminate\Http\Response
     */
    public function edit(Year $year)
    {
        $role = Role::firstOrCreate(['id' => Auth::user()->role_id]);
        if ($role->hasPermissionTo('year-edit')) {
            $lims_transport_data = $year;
            return $lims_transport_data;
        }
        else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function update(Request $request, Year $year)
    {
        $data = $request->all();
        $year = Year::find($data['year_id']);
        unset($data['year_id']);
        if($request->status == 1){
            Year::where('status', 1)->update(['status'=>0]);
        }
        $year->update($data);
        return redirect('year')->with('message', 'Financial Year updated successfully');
    }

    
    public function destroy(Year $year)
    {
        $record = Year::FindOrFail($year->id);
        $record->delete();
        return redirect('year')->with('not_permitted', 'Financial Year deleted successfully');
    }

    public function deleteBySelection(Request $request)
    {
        $records = $request['expenseIdArray'];
        array_shift($records);
        foreach ($records as $id) {
            $lims_transport_data = Source::find($id);
            $lims_transport_data->delete();
        }
        return 'Years deleted successfully!';
    }
}
