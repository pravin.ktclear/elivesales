<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hsn;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;
use App\Tax;
use App\Http\Requests\hsnRequest;


class HsnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::find(Auth::user()->role_id);
        if ($role->hasPermissionTo('tax')) {
            $lims_tax_list = Tax::where('is_active', true)->get();
            $lims_tax_all = Hsn::select('hsn.*', 'taxes.name', 'taxes.rate')
                ->with('tax')
                ->leftjoin('taxes', 'hsn.tax_id', '=', 'taxes.id')
                ->where('hsn.is_active', true)
                ->orderBy('hsn.hsnId', 'desc')->get();
            return view('hsn.create', compact('lims_tax_all', 'lims_tax_list'));
        } else
            return redirect()->back()->with('not_permitted', 'Sorry! You are not allowed to access this module');
    }

    public function create()
    {
    }

    public function store(hsnRequest $request)
    {
        $input = $request->all();
        $input['is_active'] = true;
        Hsn::create($input);
        return redirect('hsn')->with('message', 'Data inserted successfully');
    }

    
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $lims_tax_data = Hsn::findOrFail($id);
        return $lims_tax_data;
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'hsnCode' => [
                'max:255',
                Rule::unique('hsn')->where(function ($query) {
                    return $query->where('is_active', 1);
                }),
            ],

        ]);

        $input = $request->all();
        $lims_tax_data = Hsn::where('hsnId', $input['hsnId'])->first();
        $lims_tax_data->update($input);
        return redirect('hsn')->with('message', 'Data updated successfully');
    }

    public function destroy($id)
    {
        $lims_tax_data = Hsn::findOrFail($id);
        $lims_tax_data->is_active = false;
        $lims_tax_data->save();
        return redirect('hsn')->with('not_permitted', 'Data deleted successfully');
    }
    public function deleteBySelection(Request $request)
    {

        $tax_id = $request['taxIdArray'];

        foreach ($tax_id as $id) {
            $lims_tax_data = Hsn::findOrFail($id);
            $lims_tax_data->is_active = false;
            $lims_tax_data->save();
        }
        return 'Tax deleted successfully!';
    }
}
