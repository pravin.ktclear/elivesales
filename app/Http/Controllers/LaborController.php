<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Labor;
use Illuminate\Validation\Rule;
use Keygen;

class LaborController extends Controller
{

    public function index()
    {
        $lims_warehouse_all = Labor::where('is_active', true)->get();
        return view('Labor.create', compact('lims_warehouse_all'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'max:255',
                    Rule::unique('labors')->where(function ($query) {
                    return $query->where('is_active', 1);
                }),
            ],
        ]);
        $input = $request->all();
        $input['is_active'] = true;
        Labor::create($input);
        return redirect('Labor')->with('message', 'Data inserted successfully');
    }

    public function edit($id)
    {
        $lims_Labor_data = Labor::findOrFail($id);
        return $lims_Labor_data;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => [
                'max:255',
                    Rule::unique('Labors')->ignore($request->Labor_id)->where(function ($query) {
                    return $query->where('is_active', 1);
                }),
            ],
        ]);
        $input = $request->all();
        $lims_Labor_data = Labor::find($input['Labor_id']);
        $lims_Labor_data->update($input);
        return redirect('labor')->with('message', 'Data updated successfully');
    }

    public function importLabor(Request $request)
    {
        //get file
        $upload=$request->file('file');
        $ext = pathinfo($upload->getClientOriginalName(), PATHINFO_EXTENSION);
        if($ext != 'csv')
            return redirect()->back()->with('not_permitted', 'Please upload a CSV file');
        $filename =  $upload->getClientOriginalName();
        $upload=$request->file('file');
        $filePath=$upload->getRealPath();
        //open and read
        $file=fopen($filePath, 'r');
        $header= fgetcsv($file);
        $escapedHeader=[];
        //validate
        foreach ($header as $key => $value) {
            $lheader=strtolower($value);
            $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
            array_push($escapedHeader, $escapedItem);
        }
        //looping through othe columns
        while($columns=fgetcsv($file))
        {
            if($columns[0]=="")
                continue;
            foreach ($columns as $key => $value) {
                $value=preg_replace('/\D/','',$value);
            }
           $data= array_combine($escapedHeader, $columns);

           $Labor = Labor::firstOrNew([ 'name'=>$data['name'], 'is_active'=>true ]);
           $Labor->name = $data['name'];
           $Labor->phone = $data['phone'];
           $Labor->email = $data['email'];
           $Labor->address = $data['address'];
           $Labor->is_active = true;
           $Labor->save();
        }
        return redirect('labor')->with('message', 'Labor imported successfully');
    }

    public function deleteBySelection(Request $request)
    {
        $Labor_id = $request['LaborIdArray'];
        foreach ($Labor_id as $id) {
            $lims_Labor_data = Labor::find($id);
            $lims_Labor_data->is_active = false;
            $lims_Labor_data->save();
        }
        return 'Labor deleted successfully!';
    }

    public function destroy($id)
    {
        $lims_Labor_data = Labor::find($id);
        $lims_Labor_data->is_active = false;
        $lims_Labor_data->save();
        return redirect('labor')->with('not_permitted', 'Data deleted successfully');
    }
}

