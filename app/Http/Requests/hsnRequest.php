<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class hsnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hsnCode' => 'bail|required|unique:hsn|numeric',
            'tax_id' => 'bail|required|numeric',
        ];
    }
}
