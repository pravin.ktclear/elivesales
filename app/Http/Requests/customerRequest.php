<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class customerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_group_id'=>'required',
            'customer_name' => 'bail|required|regex:/^[\s\w-]*$/|min:3|max:100',
            'customer_code' => 'bail|required|regex:/^[\w-]*$/',
            'company_name' => 'bail|required|unique:customers,company_name,'.$this->company_name.'|regex:/^[\s\w-]*$/|min:3|max:100',
            'gst' => 'bail|required|regex:/^[\w-]*$/',
            'website' => 'url',
            'email' => 'bail|required|email|unique:customers,email',
            'phone_number' => 'bail|required|numeric|digits:10',
            'billing_address' => 'required',
            'shipping_address' => 'required',
            'transport' => 'required',
            'source' => 'required',
            'contact_person' => 'bail|required|regex:/^[\s\w-]*$/|min:3|max:50',
            'contact_person_mobile' => 'bail|required|numeric|digits:10',
            'contact_person_email'=>'regex:/(.+)@(.+)\.(.+)/i',
            'contact_person_mobile2' => 'numeric|digits:10',
            'contact_person_email2'=>'regex:/(.+)@(.+)\.(.+)/i',
            'user_id'=>'required|numeric',
            'postal_code'=>'bail|required|numeric',
            'city'=>'',
            'state'=>'',
            'remark' => '',
            'rating'=>'bail|required|integer|between:1,5'
        ];
    }
}
